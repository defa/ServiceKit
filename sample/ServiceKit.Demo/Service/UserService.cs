﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using ServiceKit.Aop;
using ServiceKit.Demo.Domain.Intrfaces;
using ServiceKit.Demo.Domain.Requests;
using ServiceKit.Demo.Domain.Responses;

namespace ServiceKit.Demo.Services
{
    public class UserService :IUserService
    {
        public AddUserResponse AddUser(AddUserRequest request)
        {
            return new AddUserResponse() { Result = true, SubCode = "1", SubMsg = "保存成功"+ ServiceKit.Extensions.HttpContext.Current.Request.GetDisplayUrl()};
        }
    }
}
