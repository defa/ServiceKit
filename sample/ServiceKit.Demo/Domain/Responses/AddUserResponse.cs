﻿using ServiceKit.Aop;

namespace ServiceKit.Demo.Domain.Responses
{
    public class AddUserResponse : AopResponse
    {
        public bool Result { get; set; }
    }
}
