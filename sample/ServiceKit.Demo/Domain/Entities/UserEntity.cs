﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceKit.Demo.Domain.Entities
{
    public class UserEntity:IEntity
    {
        [Required(ErrorMessage ="Id是必填项")]
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
