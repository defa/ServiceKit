﻿using ServiceKit.Demo.Domain.Requests;
using ServiceKit.Demo.Domain.Responses;
using ServiceKit.JsonRpc;
using System.ComponentModel;

namespace ServiceKit.Demo.Domain.Intrfaces
{
    [Description("用户信息接口")]
    public interface IUserService : IAopService
    {
        [RpcMethod("com.gutun.user.adduser")]
        [Description("添加用户信息")]
        AddUserResponse AddUser(AddUserRequest request);
    }
    
}
