﻿using ServiceKit.Aop;
using ServiceKit.Demo.Domain.Entities;
using ServiceKit.Demo.Domain.Responses;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ServiceKit.Demo.Domain.Requests
{
    public class AddUserRequest : AopRequest<AddUserResponse>
    {
        [Required(ErrorMessage = "Id是必填项")]
        [Description("编号")]
        public string Id { get; set; }

        [Description("用户名称")]
        public string Name { get; set; }
    }
}
