@echo off
cd /d %~dp0
SET ABS_PATH=%~dp0 
SET REL_PATH=../../../../../lib
call:MakeAbsolute REL_PATH "%ABS_PATH%"
echo 当前文件夹：%~dp0
echo lib发布文件夹 %REL_PATH%
echo dotnet pack --output %REL_PATH% -c Release --no-restore
dotnet pack --output %REL_PATH% -c Release --no-restore
pause

::----------------------------------------------------------------------------------
:: Function declarations
:: Handy to read http://www.dostips.com/DtTutoFunctions.php for how dos functions
:: work.
::----------------------------------------------------------------------------------
:MakeAbsolute file base -- makes a file name absolute considering a base path
::                      -- file [in,out] - variable with file name to be converted, or file name itself for result in stdout
::                      -- base [in,opt] - base path, leave blank for current directory
:$created 20060101 :$changed 20080219 :$categories Path
:$source http://www.dostips.com
SETLOCAL ENABLEDELAYEDEXPANSION
set "src=%~1"
if defined %1 set "src=!%~1!"
set "bas=%~2"
if not defined bas set "bas=%cd%"
for /f "tokens=*" %%a in ("%bas%.\%src%") do set "src=%%~fa"
( ENDLOCAL & REM RETURN VALUES
    IF defined %1 (SET %~1=%src%) ELSE ECHO.%src%
)
EXIT /b
