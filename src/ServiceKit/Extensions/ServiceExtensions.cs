﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceKit
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// 获取ServiceName
        /// </summary>
        public static string GetConventionalServiceName(this Type type)
        {
            var typeName = type.Name;

            if (typeName.EndsWith("Service"))
            {
                typeName = typeName.Substring(0, typeName.Length - "Service".Length);
            }

            if (typeName.Length > 1 && typeName.StartsWith("I") && char.IsUpper(typeName, 1))
            {
                typeName = typeName.Substring(1);
            }

            return typeName;
        }
    }
}
