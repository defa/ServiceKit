﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceKit
{
    /// <summary>
    /// 基础接口类
    /// </summary>
    public interface IDependency
    {
    }

    /// <summary>
    /// 实体的基类
    /// </summary>
    public interface IEntity : IDependency
    {

    }

    /// <summary>
    /// 服务的基类
    /// </summary>
    public interface IService : IDependency
    {

    }

    public interface IReturn : IDependency { }
    public interface IReturn<T> : IReturn { }
    public interface IReturnVoid : IReturn { }
}
