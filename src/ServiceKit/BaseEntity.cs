﻿using NPoco;
using System;
using System.ComponentModel;

namespace ServiceKit
{
    /// <summary>
    /// 实体的基类
    /// </summary>
    [PrimaryKey("Id", AutoIncrement = true)]
    public class BaseEntity
    {

        /// </summary>
        /// 主键
        /// </summary>
        [Column("Id")]
        [Description("主键"), NotNull]
        public virtual long Id { get; set; }

        /// </summary>
        /// 创建时间
        /// </summary>
        [Column("CreateTime")]
        [Description("创建时间"), NotNull]
        public virtual DateTime CreateTime { get; set; }

        /// </summary>
        /// 更新时间
        /// </summary>
        [Column("UpdateTime")]
        [Description("更新时间"), NotNull]
        public virtual DateTime UpdateTime { get; set; }


        public BaseEntity()
        {
            this.UpdateTime = DateTime.Now;
            this.CreateTime = DateTime.Now;
        }
    }
}
