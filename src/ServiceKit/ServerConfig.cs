﻿namespace ServiceKit
{
    public class ServerConfig
    {
        /// <summary>
        /// 服务Host
        /// </summary>
        public string ServerHost { get; set; }

        /// <summary>
        /// 模块名称
        /// </summary>
        public string ModuleName { get; set; }
    }
}
