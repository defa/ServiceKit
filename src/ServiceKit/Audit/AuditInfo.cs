﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ServiceKit.Audit
{
    [Description("审计日志")]
    public class AuditInfo
    {
        /// <summary>
        /// 应用Id
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 应用用户Id
        /// </summary>
        public string AppUId { get; set; }

        /// <summary>
        /// Url
        /// </summary>
        public string ActionUrl { get; set; }

        /// <summary>
        /// 模块名称
        /// </summary>
        public string ModuleName { get; set; }

        /// <summary>
        /// action名称
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// 服务名称
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// 方法名称
        /// </summary>
        public string MethodName { get; set; }

        /// <summary>
        /// 参数
        /// </summary>
        public string Parameters { get; set; }


        /// <summary>
        /// 输出参数
        /// </summary>
        public string Output { get; set; }

        /// <summary>
        /// 开始执行时间
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 结束执行时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 耗时
        /// </summary>
        public int ExecutionDuration { get; set; }

        /// <summary>
        /// 客户端IP
        /// </summary>
        public string ClientIpAddress { get; set; }

        /// <summary>
        /// 客户端名称
        /// </summary>
        public string ClientName { get; set; }

        /// <summary>
        /// 浏览器信息
        /// </summary>
        public string BrowserInfo { get; set; }

        /// <summary>
        /// 是否有异常
        /// </summary>
        public bool HasException { get; set; }

        /// <summary>
        /// 异常
        /// </summary>
        public string Exception { get; set; }
    }
}
