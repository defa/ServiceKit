﻿using System;

namespace ServiceKit.JsonRpc
{
    public interface IRpcClient
    {
        T For<T>() where T : IService;
        T For<T>(string moduleName, string serviceName) where T : IService;
    }

    /// <summary>
    /// MQ Rpc URl属性类
    /// </summary>
    public class RpcServiceAttribute : Attribute
    {

        /// <summary>
        /// api Url
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// MQ Rpc URl属性类
        /// </summary>
        /// <param name="_url">api Url</param>
        public RpcServiceAttribute(string _url)
        {
            this.Url = _url;
        }
    }

    /// <summary>
    /// MQ Rpc Channel模块属性类
    /// </summary>
    public class RpcModuleAttribute : Attribute
    {
        /// <summary>
        /// 模块名称
        /// </summary>
        public string Module { get; set; }

        /// <summary>
        /// MQ Rpc Channel模块属性类
        /// </summary>
        /// <param name="Module">模块名称</param>
        public RpcModuleAttribute(string Module)
        {
            this.Module = Module;
        }
    }
}
