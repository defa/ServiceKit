﻿using System;

namespace ServiceKit.JsonRpc
{
    public class RpcMethodAttribute : Attribute
    {
        public RpcMethodAttribute() { }

        public RpcMethodAttribute(string name)
        {
            this.Name = name;
        }

        public String Name { get; set; }
    }
}
