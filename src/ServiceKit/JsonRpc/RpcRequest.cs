﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceKit.JsonRpc
{
    /// <summary>
    /// 请求参数
    /// </summary>
    [JsonObject]
    public class RpcRequest
    {

		public RpcRequest(string id, string method, JToken parameters)
        {
            this.Id = id;
            this.JsonRpcVersion = RpcContants.JsonRpcVersion;
            this.Method = method;
            this.Parameters = parameters;
        }

        /// <summary>
        /// Rpc请求Id
        /// </summary>
		[JsonProperty("id", Required = Required.Default)]
        public string Id { get; private set; }
        
        /// <summary>
        /// Rpc版本，默认值是2.0
        /// </summary>
        [JsonProperty("jsonrpc")]
        public string JsonRpcVersion { get; private set; }

        /// <summary>
        /// 请求方法名称
        /// </summary>
        [JsonProperty("method", Required = Required.Always)]
        public string Method { get; private set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        [JsonProperty("params")]
        public JToken Parameters { get; private set; }
        
    }
}
