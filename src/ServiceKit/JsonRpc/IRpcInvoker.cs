﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ServiceKit.JsonRpc
{
    public interface IRpcInvoker
    {
        Task<RpcResponse> InvokeRequestAsync<T>(RpcRequest request, T service, JsonSerializerSettings jsonSerializerSettings = null);
        Task<RpcResponse> InvokeGateWayRequestAsync(RpcRequest request, IEnumerable<object> services, JsonSerializerSettings jsonSerializerSettings = null);
    }
}
