﻿using Newtonsoft.Json;

namespace ServiceKit.JsonRpc
{
    public class RpcServerConfiguration
    {
        /// <summary>
        /// JsonRpc版本 默认 2.0
        /// </summary>
        public string JsonRpcVersion { get; set; } = "2.0";

        /// <summary>
        /// 是否显示服务端异常,默认是 false
        /// </summary>
        public bool ShowServerException { get; set; } = false;

        /// <summary>
        /// JsonRpc服务url模板，默认 api/{module}/{service}
        /// </summary>
        public string Template { get; set; } = "api/{module}/{service}";

        /// <summary>
        /// 模块名称
        /// </summary>
        public string ModuleName { get; set; }

        /// <summary>
        /// Json设置
        /// </summary>
        public JsonSerializerSettings JsonSerializerSettings { get; set; }
    }
}
