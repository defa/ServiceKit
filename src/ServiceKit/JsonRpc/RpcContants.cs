﻿namespace ServiceKit.JsonRpc
{
    /// <summary>
    /// Rpc全局设置
    /// </summary>
    public class RpcContants
    {
        public const string JsonRpcVersion = "2.0";

        public static RpcServerConfiguration RpcConfig { get; set; }
    }
}
