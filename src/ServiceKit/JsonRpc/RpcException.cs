﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceKit.JsonRpc
{
    [JsonObject]
    public class RpcException : Exception
    {
        /// <summary>
        /// 错误代码
        /// </summary>
		public int ErrorCode { get; }

        /// <summary>
        /// 错误数据
        /// </summary>
        public new JToken Data { get; }
        
        public RpcException(int errorCode, string message, JToken data = null, Exception innerException = null) : base(message, innerException)
        {
            this.ErrorCode = errorCode;
            this.Data = data;
        }
    }
}
