﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceKit.JsonRpc
{
    [JsonObject]
    public class RpcResponse
    {

        public RpcResponse(string id,int code, string message)
        {
            this.Id = null;
            this.Code = code;
            this.Message = message;
            this.JsonRpcVersion = RpcContants.JsonRpcVersion;
            this.Error = null;
        }

		public RpcResponse(string id, RpcError error)
        {
            this.Id = id;
            this.Error = error;
            this.Code = 1;
            this.Message = "rpc调用失败";
            this.JsonRpcVersion = RpcContants.JsonRpcVersion;
        }

        public RpcResponse(string id, JToken result)
        {
            this.Id = id;
            this.Result = result;
            this.Code = 0;
            this.Message = "rpc调用成功";
            this.JsonRpcVersion = RpcContants.JsonRpcVersion;
        }

        /// <summary>
        /// Rpc请求Id
        /// </summary>
        [JsonProperty("id", Required = Required.AllowNull)]
        public string Id { get; private set; }

        /// <summary>
        /// Rpc版本，默认值是2.0
        /// </summary>
        [JsonProperty("jsonrpc")]
        public string JsonRpcVersion { get; private set; }

        /// <summary>
        /// 网关返回码
        /// </summary>
		[JsonProperty("code", Required = Required.Always)]
        public int Code { get; private set; }

        /// <summary>
        /// 网关返回码描述
        /// </summary>
        [JsonProperty("message", Required = Required.Default)]
        public string Message { get; private set; }
        
        /// <summary>
        /// 返回结果
        /// </summary>
        [JsonProperty("result", Required = Required.Default)]
        public JToken Result { get; private set; }

        /// <summary>
        /// 错误体
        /// </summary>
        [JsonProperty("error", Required = Required.Default)]
        public RpcError Error { get; set; }
    }
}
