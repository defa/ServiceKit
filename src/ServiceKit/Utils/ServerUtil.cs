﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace ServiceKit
{
    public class ServerUtil
    {
        private static IConfiguration configuration;

        public static IConfiguration Configuration
        {
            get
            {
                if (null == configuration)
                {
                    var builder = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json");
                    configuration = builder.Build();
                }
                return configuration;
            }
        }
    }
}
