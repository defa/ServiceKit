﻿using Newtonsoft.Json;

namespace ServiceKit.Aop
{
    public abstract class AopResponse
    {
        [JsonProperty("sub_code")]
        public string SubCode { get; set; }
        
        [JsonProperty("sub_msg")]
        public string SubMsg { get; set; }
    }
}
