using System;
using Microsoft.AspNetCore.Http;

namespace ServiceKit.Aop
{
    //https://dotnetcoretutorials.com/2017/01/05/accessing-httpcontext-asp-net-core/
    public class AopService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AopService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        
        public IHttpContextAccessor HttpContextAccessor
        {
            get { return _httpContextAccessor; }
        }
    }
}