﻿using Microsoft.AspNetCore.Mvc.ApplicationParts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ServiceKit.WebApi
{
    public class DynamicApiControllerApplicationPart : ApplicationPart, IApplicationPartTypeProvider
    {
        public DynamicApiControllerApplicationPart()
        {
            Types = Enumerable.Empty<TypeInfo>();
        }

        public DynamicApiControllerApplicationPart(IEnumerable<TypeInfo> types)
        {
            Types = types;
        }

        public IEnumerable<TypeInfo> Types { get; }

        public override string Name => "Dynamic Api Controller ApplicationPart";
    }
}
