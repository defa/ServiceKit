﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ServiceKit
{
    /// <summary>
    /// 日志存储客户端
    /// </summary>
    public class Log
    {

        private const string queuename = "Wonders.Logs.Service";

        /// <summary>
        /// 保存日志
        /// </summary>
        /// <typeparam name="T">日志类型</typeparam>
        /// <param name="log">日志实例</param>
        /// <param name="typename">默认可不填</param>
        public static void Save<T>(T log, string typename = null)
        {
            try
            {
                var context = JsonConvert.SerializeObject(new
                {
                    LogType = typename ?? typeof(T).Name,
                    LogStr = JsonConvert.SerializeObject(log),
                });
                QueuesCall(queuename, context);
            }
            catch (Exception ex)
            {
            }
        }

        private static ConnectionFactory factory
        {
            get
            {
                MQConfig config = ServerUtil.Configuration.GetSection("MQConfig").Get<MQConfig>();
                if (null != config)
                {
                    return new ConnectionFactory()
                    {
                        HostName = config.HostName,
                        Port = config.Port,
                        UserName = config.UserName,
                        Password = config.Password,
                    };
                }
                else
                {
                    throw new Exception("MQConfig节点未配置");
                }

            }
        }

        public static void QueuesCall(string queuename, string context, bool durable = false)
        {
            Task.Run(() =>
            {
                using (var connection = factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {

                        channel.QueueDeclare(queuename,
                                             durable: durable,
                                             exclusive: false,
                                             autoDelete: false,
                                             arguments: null);

                        var body = Encoding.UTF8.GetBytes(context);
                        var properties = channel.CreateBasicProperties();
                        properties.Persistent = true;
                        channel.BasicPublish(exchange: "",
                                            routingKey: queuename,
                                            basicProperties: properties,
                                            body: body);
                    }
                }
            });
        }

        /// <summary>
        /// 保存日志
        /// </summary>
        /// <param name="LogType">日志类型</param>
        /// <param name="LogStr">日志记录</param>
        public static void Save(string LogType, string LogStr)
        {
            Save(new { LogType, LogStr, }, "LogEntity");
        }

        /// <summary>
        /// 保存CacheKey
        /// </summary>
        /// <param name="CacheKey">生成的Key</param>
        /// <param name="CacheKeyDesc">Key的描述</param>
        public static void SaveCacheKey(string CacheKey, string CacheKeyDesc)
        {
            Save(new { CacheKey, CacheKeyDesc, }, "LogCacheKey");
        }
    }
}
