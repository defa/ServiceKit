﻿using ServiceKit.JsonRpc;
using System;

namespace ServiceKit
{
    public class Cache
    {
        private static ICacheService _cache;
        static Cache()
        {
            _cache = RpcClient.For<ICacheService>();
        }
        /// <summary>
        /// 根据key删除
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Remove(string key)
        {
            try
            {
                return _cache.Remove(key);
            }
            catch (Exception)
            {

                return false;
            }

        }
        /// <summary>
        /// 根据key删除
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Delete(string key)
        {
            try
            {
                return _cache.Delete(key);
            }
            catch (Exception)
            {

                return false;
            }

        }
        /// <summary>
        /// 根据key查找
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string Get(string key)
        {
            try
            {
                return _cache.Get(key);
            }
            catch (Exception)
            {

                return null;
            }

        }
        /// <summary>
        /// 根据key缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expireMinutes"></param>
        /// <returns></returns>
        public static bool Set(string key, string value, int expireMinutes = 0)
        {
            try
            {
                return _cache.Set(key, value, expireMinutes);
            }
            catch (Exception)
            {

                return false;
            }

        }
        /// <summary>
        /// 设置指定时间失效缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <param name="minutes">分钟</param>
        /// <returns></returns>
        public static bool SetExpiry(string key, string obj, int minutes)
        {
            try
            {
                return _cache.Set(key, obj, minutes);
            }
            catch (Exception)
            {

                return false;
            }

        }
        /// <summary>
        /// 判断是否包含key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool HasKey(string key)
        {
            try
            {
                return _cache.HasKey(key);
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
