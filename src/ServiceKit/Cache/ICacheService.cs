﻿using ServiceKit.JsonRpc;

namespace ServiceKit
{
    [RpcModule("Cache")]
    [RpcService("api/cache/cache")]
    public interface ICacheService : IService
    {
        /// <summary>
        /// 根据key删除
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool Remove(string key);
        /// <summary>
        /// 根据key删除
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool Delete(string key);
        /// <summary>
        /// 根据key查找
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        string Get(string key);
        /// <summary>
        /// 根据key缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expireMinutes"></param>
        /// <returns></returns>
        bool Set(string key, string value, int expireMinutes = 0);
        /// <summary>
        /// 设置指定时间失效缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <param name="minutes">分钟</param>
        /// <returns></returns>
        bool SetExpiry(string key, string obj, int minutes);
        /// <summary>
        /// 判断是否包含key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool HasKey(string key);
    }
}
