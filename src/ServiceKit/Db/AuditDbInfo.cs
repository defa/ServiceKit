﻿using System;

namespace ServiceKit
{
    public class AuditDbInfo
    {
        /// <summary>
        /// 开始执行时间
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 结束执行时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 耗时
        /// </summary>
        public int ExecutionDuration { get; set; }

        /// <summary>
        /// 数据源
        /// </summary>
        public string DataSource { get; set; }

        /// <summary>
        /// 数据库
        /// </summary>
        public string DataBase { get; set; }

        /// <summary>
        /// SQL语句
        /// </summary>
        public string SQL { get; set; }

        /// <summary>
        /// 是否有异常
        /// </summary>
        public bool HasException { get; set; }

        /// <summary>
        /// 异常
        /// </summary>
        public string Exception { get; set; }
    }
}
