﻿using System;
using System.Reflection;

namespace ServiceKit
{
    /// <summary>
    /// 属性或参数非空属性
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Parameter)]
    public class NotNullAttribute : Attribute
    {
    }

    public static class PropertyInfoExtentions
    {

        /// <summary>
        /// 判断属性是否可空
        /// </summary>
        /// <param name="prperty"></param>
        /// <returns></returns>
        public static bool IsNotNull(this PropertyInfo prperty)
        {
            var attrs = prperty.GetCustomAttributes(true);
            foreach (var obj in attrs)
            {
                var attr = obj as NotNullAttribute;
                if (attr != null) return true;
            }
            return false;
        }

        /// <summary>
        /// 判断参数是否可空
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static bool IsNotNull(this ParameterInfo param)
        {
            var attrs = param.GetCustomAttributes(true);
            foreach (var obj in attrs)
            {
                var attr = obj as NotNullAttribute;
                if (attr != null) return true;
            }
            return false;
        }

    }
}
