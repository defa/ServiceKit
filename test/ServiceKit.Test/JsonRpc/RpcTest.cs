using Microsoft.Extensions.Options;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ServiceKit.JsonRpc;
using System;
using Xunit;

namespace ServiceKit.Test
{
    public class RpcTest
    {
        private DefaultRpcInvoker GetInvoker()
        {
            var options = new Mock<IOptions<RpcServerConfiguration>>();
            var config = new RpcServerConfiguration();
            config.ShowServerException = true;
            options
                .SetupGet(o => o.Value)
                .Returns(config);

            return new DefaultRpcInvoker(options.Object);
        }

        [Fact]
        public void Test1()
        {
            string json = "{'method':'Create','params':['aa','aa']}";
            var rpcRequest = JsonConvert.DeserializeObject<RpcRequest>(json);
            var flag= GetInvoker().InvokeRequestAsync<IUserService>(rpcRequest, new UserService()).Result;
            if (flag.Code == 200)
            {
                Assert.Equal("1", flag.Result.ToObject<string>());
            }
            else
            {
                throw new Exception(flag.Message);
            }
        }

        

        [Fact]
        public void Test11()
        {
            string json = "{'method':'Create','params':['aa',{}]}";
            var rpcRequest = JsonConvert.DeserializeObject<RpcRequest>(json);
            var flag = GetInvoker().InvokeRequestAsync<IUserService>(rpcRequest, new UserService()).Result;
            if (flag.Code == 200)
            {
                Assert.Equal("1", flag.Result.ToObject<string>());
            }
            else
            {
                throw new Exception(flag.Message);
            }
        }

        [Fact]
        public void Test12()
        {
            string json = "{'method':'Create','params':['aa','aa','22']}";
            var rpcRequest = JsonConvert.DeserializeObject<RpcRequest>(json);
            var flag = GetInvoker().InvokeRequestAsync<IUserService>(rpcRequest, new UserService()).Result;
            if (flag.Code == 200)
            {
                Assert.Equal("1", flag.Result.ToObject<string>());
            }
            else
            {
                throw new Exception(flag.Message);
            }
        }

        [Fact]
        public void Test2()
        {
            string json = "{'method':'Create','params':{'name':'aa','password':'aa'}}";
            var rpcRequest = JsonConvert.DeserializeObject<RpcRequest>(json);
            var flag = GetInvoker().InvokeRequestAsync<IUserService>(rpcRequest, new UserService()).Result;
            if (flag.Code == 200)
            {
                Assert.Equal("1", flag.Result.ToObject<string>());
            }
            else
            {
                throw new Exception(flag.Message);
            }
        }

        [Fact]
        public void Test21()
        {
            string json = "{'method':'Create','params':{'name':'aa','password':{}}}";
            var rpcRequest = JsonConvert.DeserializeObject<RpcRequest>(json);
            var flag = GetInvoker().InvokeRequestAsync<IUserService>(rpcRequest, new UserService()).Result;
            if (flag.Code == 200)
            {
                Assert.Equal("1", flag.Result.ToObject<string>());
            }
            else
            {
                throw new Exception(flag.Message);
            }
        }

        [Fact]
        public void Test22()
        {
            string json = "{'method':'Create','params':{'name':'aa','password':'aa','age':12}}";
            var rpcRequest = JsonConvert.DeserializeObject<RpcRequest>(json);
            var flag = GetInvoker().InvokeRequestAsync<IUserService>(rpcRequest, new UserService()).Result;
            if (flag.Code == 200)
            {
                Assert.Equal("1", flag.Result.ToObject<string>());
            }
            else
            {
                throw new Exception(flag.Message);
            }
        }

        [Fact]
        public void Test3()
        {
            
            string json = "{'method':'Create','params':{'name':'aa','age':'aa'}}";
            var rpcRequest = JsonConvert.DeserializeObject<RpcRequest>(json);
            RpcResponse flag = GetInvoker().InvokeRequestAsync<IUserService>(rpcRequest, new UserService()).Result;
            if (flag.Code==200)
            {
                Assert.Equal("2", flag.Result.ToObject<string>());
            }
            else
            {
                throw new Exception(flag.Message);
            }
        }

        [Fact]
        public void Test31()
        {
            string json = "{'method':'Create','params':['aa',12]}";
            var rpcRequest = JsonConvert.DeserializeObject<RpcRequest>(json);
            var flag = GetInvoker().InvokeRequestAsync<IUserService>(rpcRequest, new UserService()).Result;
            if (flag.Code == 200)
            {
                Assert.Equal("2", flag.Result.ToObject<string>());
            }
            else
            {
                throw new Exception(flag.Message);
            }
        }
    }

    public interface IUserService : IService
    {
        string Create(string name, string password);
        string Create(string name, int age);
    }

    public class UserService : IUserService
    {
        public string Create(string name, string password)
        {
            return "1";
        }

        public string Create(string name, int age)
        {
            return "2";
        }
    }
}
